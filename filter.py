#!/usr/bin/env python3

"""
This script filters the provider list and creates a list of extracted providers.
"""

from datetime import datetime, timedelta

from common import *

def create_simple_output(jid, properties):
	"""Creates a simple output for the properties of a provider consisting only of the information relevant to the client.

	Parameters
	----------
	jid : str
		JID of the provider
	properties : dict
		properties of the provider

	Returns
	-------
	dict
		only consisting of relevant properties
	"""

	del properties["lastCheck"]
	new_properties = {"jid": jid}

	for property_name, property_content in properties.items():
		if "content" in property_content:
			new_properties[property_name] = property_content["content"]

	return new_properties

def filter_provider(category, properties):
	"""Filters properties by a passed category.

	Parameters
	----------
	category : common.Category
		category used for filtering
	properties : dict
		properties of the provider

	Returns
	-------
	bool
		whether the provider belongs to the category
	"""

	if category == Category.USABLE_FOR_AUTOCOMPLETE:
		return True
	if (
		category == Category.MANUALLY_SELECTABLE and
		(
			properties["inBandRegistration"] or
			len(properties["registrationWebPage"]) != 0
		) and
		properties["ratingXmppComplianceTester"] >= 90 and
		"A" in properties["ratingImObservatory"] and
		properties["maximumHttpFileUploadFileSize"] >= 0 and
		properties["maximumHTTPFileUploadTotalSize"] >= 0 and
		properties["maximumMessageArchiveManagementStorageTime"] >= 0
	):
		return True
	if (
		category == Category.AUTOMATICALLY_CHOSEN and
		properties["inBandRegistration"] and
		properties["ratingXmppComplianceTester"] >= 90 and
		"A" in properties["ratingImObservatory"] and
		(
			properties["maximumHttpFileUploadFileSize"] == 0 or
			properties["maximumHttpFileUploadFileSize"] >= 20
		) and
		(
			properties["maximumHTTPFileUploadTotalSize"] == 0 or
			properties["maximumHTTPFileUploadTotalSize"] >= 100
		) and
		(
			properties["maximumMessageArchiveManagementStorageTime"] == 0 or
			properties["maximumMessageArchiveManagementStorageTime"] >= 7
		) and
		properties["professionalHosting"] and
		properties["freeOfCharge"] and
		len(properties["imprint"]) != 0 and
		len(properties["serverLocations"]) != 0 and
		(
			properties["groupChatSupport"] or
			properties["adminChatSupport"] or
			properties["emailSupport"]
		) and
		datetime.fromisoformat(properties["onlineSince"]) < datetime.now() - timedelta(365)
	):
		return True
	return False

if __name__ == "__main__":
	arguments = ApplicationArgumentParser().parse_args()
	logging.basicConfig(level=logging.INFO, format="%(levelname)-8s %(message)s")
	
	category = arguments.category

	with open(JSON_FILE_PATH, "r") as json_file:
		try:
			providers = json.load(json_file)
			filtered_providers = []

			for jid, properties in providers.items():
				properties = create_simple_output(jid, properties)
				if filter_provider(category, properties):
					filtered_providers.append(properties)

			# A newline is appended because Python's JSON module does not add one.
			formatted_json_string = json.dumps(filtered_providers, indent=OUTPUT_INDENTATION) + "\n"

			json_file_path_with_suffix = "%s-%s.json" % (JSON_FILE_PATH.split(".")[0], category.value)
			with open(json_file_path_with_suffix, "w") as filtered_json_file:
				filtered_json_file.seek(0)
				filtered_json_file.write(formatted_json_string)
				filtered_json_file.truncate()
				logging.info("'%s' has been created" % json_file_path_with_suffix)
		except json.decoder.JSONDecodeError as e:
			logging.error("'%s' has invalid JSON syntax: %s in line %s at column %s" % (JSON_FILE_PATH, e.msg, e.lineno, e.colno))
