#!/usr/bin/env python3

"""
This script validates the provider list and applies a consistent format to it.

It is intended as a Git pre-commit hook.
"""

import logging
import sys
import json

# Set "logging.DEBUG" for debug output.
LOG_LEVEL = logging.INFO

JSON_FILE_PATH = "providers.json"
OUTPUT_INDENTATION = "\t"

logging.basicConfig(level=logging.INFO, format="%(levelname)-8s %(message)s")

with open(JSON_FILE_PATH, "r+") as json_file:
	try:
		original_json_string = json_file.read()
		
		# A newline is appended because Python's JSON module does not add one.
		formatted_json_string = json.dumps(json.loads(original_json_string), indent=OUTPUT_INDENTATION) + "\n"
		
		if (original_json_string == formatted_json_string):
			logging.debug("'%s' is already correctly formatted" % JSON_FILE_PATH)
		else:
			json_file.seek(0)
			json_file.write(formatted_json_string)
			json_file.truncate()
			logging.info("'%s' has been formatted: Stage changed lines by 'git add --patch %s' and run 'git commit' again" % (JSON_FILE_PATH, JSON_FILE_PATH))
			sys.exit(1)
	except json.decoder.JSONDecodeError as e:
		logging.error("'%s' has invalid JSON syntax: %s in line %s at column %s" % (JSON_FILE_PATH, e.msg, e.lineno, e.colno))
		sys.exit(1)
